console.log("Hello World");

//An array in programming was simply a list of data

//Example
let studentNumA = '2020-1923';
let studentNumB = '2020-1924';
let studentNumC = '2020-1925';
let studentNumD = '2020-1926';
let studentNumE = '2020-1927';

//much better
let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'];

//Arrays
/*
	Arrays used to store multiple values in a single variable
	They are declared using square brackets ([]) also known as "Array Literals"

	Syntax:

	let/const arrayName = [elementA, elementB, elementC,....]
*/

//Common examples of Arrays

let grade = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'ASUS', 'Lenovo', 'NEO', 'RedFox', 'Gateway', 'Toshiba', 'Fujitsu'];
console.log(grade);
console.log(computerBrands);

//Possible use of an Array but not recommended

let mixedArray = [12, 'Asus', null, undefined, {}];
console.log(mixedArray);

//Alternative way to write arrays

let myTasks = [
	'drink html',
	'eat JavaScript',
	'inhale CSS',
	'bake sass'
];
console.log(myTasks);

//Mini Activity
let tasks = ['cooking', 'sleeping', 'reading', 'working', 'studying'];
let capitalCities = ['Muscat', 'Oslo', 'Tegucigalpa', 'Nairobi'];
console.log(tasks);
console.log(capitalCities);

//create arrays with values from variables

let city1 = 'Tokyo';
let city2 = 'Manila';
let city3 = 'Jakarta';

let cities = [city1,city2,city3];
console.log(cities);

//length property
//The .length property allows us to get and set the total number of items in an array

console.log(myTasks.length);//.length - it shows the number of items in an arrays in the console
console.log(cities.length);

let blankArr = []; //it will return 0
console.log(blankArr.length);

//length property can also be used with strings
//some array methods and properties can also be used with strings

let fullName = 'Jamie Noble'; 
console.log(fullName.length); //.length will show how many characters are there in a string including the space

//length property can also set the total number of items in an array, meaning we can actually DELETE the last item in the array or shorten the array by simply updating the length property of an array
myTasks.length = myTasks.length-1; //count the items of array minus 1
console.log(myTasks.length);
console.log(myTasks); //it will not show the last array

//another example using decrementation in Array
cities.length--; //other way of deleting an array using decrement
console.log(cities);

fullName.length = fullName.length-1; //no changes for string
console.log(fullName.length);
fullName.length--;
console.log(fullName);

let theBeatles = ["John","Paul","Ringo","George"];
/*theBeatles.length++;*/ //result: empty string
theBeatles[4] ='Cardo';
theBeatles[theBeatles.length] = 'Ely'; //index 4
theBeatles[theBeatles.length] = 'Chito'; //index 5
theBeatles[theBeatles.length] = 'MJ'; //index 6
//another way of adding an array
console.log(theBeatles);

//Mini Activity
let theTrainers = ['Ash'];
function addTrainers(trainer){
	theTrainers[theTrainers.length] = trainer;
	console.log(theTrainers);
}
addTrainers('Misty');
addTrainers('Brock');


//Reading from Arrays
console.log(grade[0]); //98.5
console.log(computerBrands[3]); //NEO
//accessing an array element that does not exist will retun undefined
console.log(grade[20]); //undefined because there is still no value

let lakersLegends = ['Kobe','Shaq','LeBron','Magic','Karim'];
console.log(lakersLegends[1]); //2nd item array
console.log(lakersLegends[3]); //4 item array

//Array inside a variable
let currentLaker = lakersLegends[2];
console.log(currentLaker);
console.log(currentLaker);

console.log('Array before reassignment');
console.log(lakersLegends);
lakersLegends[2]='Paul Gasol';
console.log('Array after reassignment');
console.log(lakersLegends);

//Mini Activity
function findBlackMamba(index){
	return lakersLegends[index]
}
let blackMamba = findBlackMamba(0);
console.log(blackMamba);

//Mini Activity
let food = ['FC','Spag','Takoyaki','Siomai']
console.log(food);
food[3]='Pansit';
console.log(food);

//Accessing the last element of an Array

let bullslegend = ["Jordan",'Pippen','Rodman','Rose', 'kkuoc'];
let lastElementIndex = bullslegend.length-1;

console.log(lastElementIndex);//4
console.log(bullslegend.length);

console.log(bullslegend[lastElementIndex]);

console.log(bullslegend[bullslegend.length-1]);

//Adding Items into array
let newArr = [];
console.log(newArr[0]);

newArr[0] = 'Coud Strife';
console.log(newArr);

console.log(newArr[1]); //undefined since there is still no value

newArr[1]='Tifa Lockheart';
console.log(newArr);

/*newArr[newArr.length-1] = 'Aerith Gainsborough';//will replace an item
console.log(newArr);
*/
newArr[newArr.length]='Barret Wallace';
console.log(newArr);


//Looping over an array
for (let index = 0; index < newArr.length; index++){
	console.log(newArr[index])
}

//use array of numbers
let numArr = [5,12,30,46,40];
for (let index = 0; index < numArr.length; index++){
	if (numArr[index] % 5 ===0){
		console.log(numArr[index] + ' is divisible by 5')
	} else {
		console.log(numArr[index] + " is not divisible by 5")
	}
}

//Multi dimensional arrays
/*
	-multi-dimensional arrays are useful for storing complex data structures
	-a practical application of this is to help visualize or create real world objects
*/

let chessBoard = [

	['a1','b1','c1','d1','e1','f1','g1','h1'],
	['a2','b2','c2','d2','e2','f2','g2','h2'],
	['a3','b3','c3','d3','e3','f3','g3','h3'],
	['a4','b4','c4','d4','e4','f4','g4','h4'],
	['a5','b5','c5','d5','e5','f5','g5','h5'],
	['a6','b6','c6','d6','e6','f6','g6','h6'],
	['a7','b7','c7','d7','e7','f7','g7','h7']

];

console.log(chessBoard);

console.log(chessBoard[1][4]);
console.log('Pawn moves to: ' + chessBoard[1][5]); //f2

